/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2020 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::vanRijnEntrainmentFvPatchScalarField

Description
    This boundary condition provides contration gradient based on the 
    van Rijn Entrainment pick up function  condition,
    calculated as:

        \f[
            \snGrad  C = -P_k / (nu_{t}/P_{T}) for \theta > \theta_{c}
            \snGrad  C = 0 for \theta < \theta_{c}
        \f]

    where
    \vartable
        C      | sediment concentration [unitsless]
        P_k    | van Rijn Entrainment function [m/s]
        nu_{t} | eddy viscosity [m2/s]
        P_{T}  | Prandlt number [unitsless]
    \endtable


// TO BE COMPLETED
Usage
    \table
        Property   | Description                  | Req'd? | Default
        scalarData | single scalar value          | yes    |
        data       | single scalar value          | yes    |
        fieldData  | scalar field across patch    | yes    |
        timeVsData | scalar function of time      | yes    |
        wordData   | word, eg name of data object | no     | wordDefault
    \endtable

    Example of the boundary condition specification:
    \verbatim
    <patchName>
    {
        type       vanRijnEntrainment;
        scalarData -1;
        data       1;
        fieldData  uniform 3;
        timeVsData table (
                             (0 0)
                             (1 2)
                          );
        wordName   anotherName;
        value      uniform 4; // optional initial value
    }
    \endverbatim

SourceFiles
    vanRijnEntrainmentFvPatchScalarField.C

\*---------------------------------------------------------------------------*/

#ifndef BC_H
#define BC_H

#include "fixedValueFvPatchFields.H"
#include "Function1.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
     Class vanRijnEntrainmentFvPatchScalarField Declaration
\*---------------------------------------------------------------------------*/

class vanRijnEntrainmentFvPatchScalarField
:
    public fixedValueFvPatchScalarField
{
    // Private Data

        // Gradient field
        scalarField gradient_;

        // Wall shear stress field
        scalarField wallShearStress_;

        // Calibration parameters
        const scalar alpha_;
        const scalar beta_;
        const scalar gamma_;

        // Water density
        // const dimensionedScalar rho_;
        const scalar rho_;

        // Sediment density
        // const dimensionedScalar rhos_;
        const scalar rhos_;

        // Eddy viscosity
        scalarField nut_;

        // Prandlt Number
        const scalar Pt_;

        // Gravitational acceleration
        // const dimensionedScalar g_;
        const scalar g_;

        // Critical shields parameter
        const scalar thetac_;

        // Particle diameter
        // const dimensionedScalar d_;
        const scalar d_;


    // Private Member Functions

        // Calculate shields parameter
        scalarField shieldsParameter();

        // Calculates non-dimensional diameter
        scalarField D();

        // Calculates excess of shear stress
        scalarField T();

        // Calculates pick up function
        scalarField Pk();

        


public:

    //- Runtime type information
    TypeName("vanRijnEntrainment");


    // Constructors

        //- Construct from patch and internal field
        vanRijnEntrainmentFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        vanRijnEntrainmentFvPatchScalarField
        (
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given fixedValueTypeFvPatchField
        //  onto a new patch
        vanRijnEntrainmentFvPatchScalarField
        (
            const vanRijnEntrainmentFvPatchScalarField&,
            const fvPatch&,
            const DimensionedField<scalar, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Copy constructor
        vanRijnEntrainmentFvPatchScalarField
        (
            const vanRijnEntrainmentFvPatchScalarField&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchScalarField> clone() const
        {
            return tmp<fvPatchScalarField>
            (
                new vanRijnEntrainmentFvPatchScalarField(*this)
            );
        }

        //- Copy constructor setting internal field reference
        vanRijnEntrainmentFvPatchScalarField
        (
            const vanRijnEntrainmentFvPatchScalarField&,
            const DimensionedField<scalar, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchScalarField> clone
        (
            const DimensionedField<scalar, volMesh>& iF
        ) const
        {
            return tmp<fvPatchScalarField>
            (
                new vanRijnEntrainmentFvPatchScalarField
                (
                    *this,
                    iF
                )
            );
        }


    // Member Functions

        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();


        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


#endif

// ************************************************************************* //
